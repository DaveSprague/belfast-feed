var db = require('../config/database');
const fs = require('fs');
const fastcsv = require("fast-csv");
const CsvParser = require("json2csv").Parser;
var networkUtil = require('../utils/networkUtil');

require('dotenv').config({ path: __dirname + '/.env' })
var base_url = process.env['BASE_URL']

const getLatestFeeds = (num_feeds) => db.query('SELECT * FROM feeds order by feed_id desc LIMIT $1', [num_feeds]).then(response => response.rows)
.catch((err) => {
    console.log("couldn't find feed_id for that key!");
  });

const getJSON = (feed_id) => {
    
db.query('SELECT * FROM measurements WHERE feed_id = $1', [feed_id], (err, results) => {
    if (err) throw err
    res.status(200).json(restructureJSON(feed_pubkey, feed_name, results.rows));

});

}

exports.getDefault = function(req, res, next) { // NOW BY PUB_KEY

    //var feed_pubkey = req.params.feed_pubkey;
     //use the IP address for the feed link; change this once we have a fixed URL:
     //var ip = networkUtil.getIp();
    var numFeeds = 6;
    var numDays = 4;
    
     getLatestFeeds(numFeeds)
     .then((feeds) => {
        console.log(feeds);
        
        var allfeeddata = [];

        var shortNames = [];

        var itemsProcessed = 0;

        var colors = ["red","blue","green","yellow","purple","black","red"];

        feeds.forEach(feed => {
            var feedkey = feed.public_key;
            var feed_id = feed.feed_id;

            //var feedurl = 'http://192.168.1.163:3006/co2/data/'+feedkey+'/json/';
            console.log(feed_id);
            //allfeeddata.push(feed_id);
            //itemsProcessed++;
            
            
    
            //db.query('SELECT * FROM measurements WHERE feed_id = $1', [feed_id], (err, results) => {
            db.query("SELECT * FROM measurements WHERE feed_id = $1 AND created >= now() - '3 days'::interval", [feed_id], (err, results) => {
                if (err) {
                    console.log(err);
                    throw err;
                }
                else {
                //console.log("query:"+feed_id+"; itemsProcessed="+itemsProcessed+"; feeds.length="+feeds.length);
                var shortName = feedkey.substring(0,2);
                shortNames.push(shortName)
                allfeeddata.push({"feed_name":shortName,"feed_pubkey":feedkey,"color":colors[itemsProcessed],"data":results.rows});
                itemsProcessed++;
                console.log("itemsProcessedInternal="+itemsProcessed+"; feeds.length="+feeds.length);
                //res.status(200).json(restructureJSON(feed_pubkey, feed_name, results.rows));
                if(itemsProcessed=== feeds.length) {
                    console.log(allfeeddata);
                    //res.render('manage_feedmap',{short_map_url:short_map_url,bayoudata:allfeeddata,feedmap_pubkey:feedmap_params.public_key,private_key:private_key,feedmap_name:feedmap_params.name,map_url:feedmap_params.map_url,feeds:feedmap_feeds,base_url:base_url,colors:colors});
                    res.render('overlay',{shortNames:shortNames,feeds:feeds,numFeeds:feeds.length,bayoudata:allfeeddata,base_url:base_url,colors:colors});
                }
                }
            });

            console.log("itemsProcessed="+itemsProcessed+"; feeds.length="+feeds.length);

            
    
        });


     }).catch((err) => {
        console.log(err);
       });
}

