var db = require('../config/database');
const fs = require('fs');
const fastcsv = require("fast-csv");
const CsvParser = require("json2csv").Parser;
var networkUtil = require('../utils/networkUtil');

require('dotenv').config({ path: __dirname + '/.env' })
var base_url = process.env['BASE_URL']

const getFeedDetails = (feed_pubkey) => db.query('SELECT * FROM feeds WHERE public_key = $1', [feed_pubkey]).then(response => response.rows[0])
.catch((err) => {
    console.log("couldn't find feed_id for that key!");
  });

function clean(obj) {
for (var propName in obj) {
    if (obj[propName] === null || obj[propName] === undefined) {
    delete obj[propName];
    }
}
return obj
}

  
const restructureJSON = (feed_pubkey, feed_name, inputJSON) => {
    var structjson=[];
var alldata = inputJSON;
            alldata.forEach(element => {
                //console.log(element);
                var id = element.id;
                var timestamp = element.timestamp;
                //var feed_key = feed_pubkey;
                //need to make this non-manual
                var parameters = {"temperature_c":element.temperature_c, "humidity_rh" :element.humidity_rh, "distance_meters" :element.distance_meters, "pressure_mbar" :element.pressure_mbar, "battery_volts" :element.battery_volts, "gps_lat" :element.gps_lat, "gps_lon" :element.gps_lon, "gps_alt" :element.gps_alt, "distance_meters_1" :element.distance_meters_1, "distance_meters_2" :element.distance_meters_2, "distance_meters_3" :element.distance_meters_3, "temperature_c_1" :element.temperature_c_1, "temperature_c_2":element.temperature_c_2, "temperature_c_3" :element.temperature_c_3, "voltage_1" :element.voltage_1, "voltage_2" :element.voltage_2, "voltage_3" :element.voltage_3, "aux_1" :element.aux_1, "aux_2" :element.aux_2, "aux_3" :element.aux_3, "log" :element.log, "rssi": element.rssi, "node_id": element.node_id, "node_name": element.node_name}

                clean(parameters);

                //var parameters = {"distance_meters":element.distance_meters};
                structjson.push({"id":id, "timestamp":timestamp,parameters});
            });
   return({"feed_pubkey":feed_pubkey,"feed_name":feed_name,"data": structjson});
}

exports.getJSON = (req,res,next) =>  {

    //var parameters = ["id","tempc","humidity","mic","auxpressure","auxtempc","aux001","aux002","created"];

    var feed_pubkey = String(req.params.feed_pubkey);

    var limit = req.params.limit;

    if (limit > "") {
        // do something with the id
        limit=parseInt(limit);
        console.log("got a param!");
    } else {
        limit=2000;
    }

    //limit = 10;

    getFeedDetails(String(req.params.feed_pubkey))
    .then((feed_params) => {

    var feed_id = feed_params.feed_id;
    var feed_name = feed_params.name;

    var quer = 'SELECT * FROM (SELECT * from measurements where feed_id = $1 ORDER BY id DESC LIMIT $2) AS _ ORDER BY id ASC';
    
    db.query(quer, [feed_id,limit], (err, results) => {
    
    //db.query('SELECT * FROM measurements WHERE feed_id = $1', [feed_id], (err, results) => {

            if (err) {
                res.status(500).json({success: false, error:'Sorry, error'});
            } else {
            res.status(200).json(restructureJSON(feed_pubkey, feed_name, results.rows));
            }    
        });
    })
    .catch((err) => {
       console.log("couldn't get measurements for that feed_id!");
       res.status(500).json({success: false, error:'Sorry, error'});
      });
   
}

exports.getPage = function(req, res, next) { // NOW BY PUB_KEY

    /*
    if (Object.keys(req.query).length === 0) {
        console.log("no url params!");
    }
    else {
        console.log("url params!");
    }
    */

    var plot_param = req.query.plot_param;
    var limit = req.query.limit;

    if (plot_param > "") {
        // do something with the id
        console.log("got a param!");
    } else {
        plot_param = "distance_meters";
        console.log("plot distance by default");
    }

    if (limit > "") {
        // do something with the id
        limit=parseInt(limit);
        console.log("got a param!");
    } else {
        limit=2000;
    }

    var feed_pubkey = req.params.feed_pubkey;
     //use the IP address for the feed link; change this once we have a fixed URL:
     var ip = networkUtil.getIp();

     getFeedDetails(String(req.params.feed_pubkey))
     .then((feed_params) => {
        console.log(feed_params);
    res.render('data',{feed_pubkey:feed_params.public_key,feed_name:feed_params.name,base_url:base_url,plot_param:plot_param,limit:limit});
     }).catch((err) => {
        res.status(400).send("Couldn't fetch that feed!");
        console.log("Something went wrong with getPage!");
       });
}

exports.getMap = function(req, res, next) { // NOW BY PUB_KEY

    var feed_pubkey = req.params.feed_pubkey;
     //use the IP address for the feed link; change this once we have a fixed URL:
     var ip = networkUtil.getIp();

     getFeedDetails(String(req.params.feed_pubkey))
     .then((feed_params) => {
        console.log(feed_params);
    res.render('map',{feed_pubkey:feed_params.public_key,feed_name:feed_params.name,base_url:base_url});
     }).catch((err) => {
        res.status(400).send("Couldn't fetch that feed!");
        console.log("Something went wrong with getPage!");
       });
}


exports.getCSV = function(req, res, next) {  // NOW BY PUB KEY

    var feed_pubkey = String(req.params.feed_pubkey);

    var limit = req.params.limit;

    if (limit > "") {
        // do something with the id
        limit=parseInt(limit);
        console.log("got a param!");
    } else {
        limit=2000;
    }

    getFeedDetails(String(req.params.feed_pubkey))
    .then((feed_params) => {
        var feed_id = feed_params.feed_id;

    console.log("feed_pubkey",feed_pubkey);
    console.log(feed_id);

    db.query('SELECT * FROM measurements WHERE feed_id = $1', [feed_id], (err, response) => {

    if (err) {
    console.log("db.query()", err.stack);
    res.status(400).send("Couldn't fetch that feed!");
    }
    
    if (response) {
    
        var ws = fs.createWriteStream('measurements.csv');

        console.log(response);

        if (response.rows.length>0){
    const jsonData = JSON.parse(JSON.stringify(response.rows));
    console.log("\njsonData:", jsonData)
    
    const csvFields = ["id", "feed_id", "created","celcius"];

    const csvParser = new CsvParser({ csvFields });
    const csvData = csvParser.parse(jsonData);

    res.setHeader("Content-Type", "text/csv");
    res.setHeader("Content-Disposition", "attachment; filename=measurements.csv");

    //res.status(200).end(csvData);
    return res.status(200).send(csvData);
    } else {
        res.status(400).send('No data yet for this feed.\n' );
    }
    }
});
    })
    .catch((err) => {
        console.log("couldn't download csv!");
        res.status(400).send("Couldn't fetch that feed!");
       });

}

exports.getKeysAsCSV = function(req, res, next) {  // NOW BY PUB KEY

    var feed_pubkey = String(req.params.feed_pubkey);

    getFeedDetails(String(req.params.feed_pubkey))
    .then((feed_params) => {
        var feed_id = feed_params.feed_id;

    console.log("feed_pubkey",feed_pubkey);
    console.log(feed_id);
    
        var ws = fs.createWriteStream('measurements.csv');

        console.log(response);

        if (response.rows.length>0){
    const jsonData = JSON.parse(JSON.stringify(response.rows));
    console.log("\njsonData:", jsonData)
    
    const csvFields = ["id", "feed_id", "created","celcius"];

    const csvParser = new CsvParser({ csvFields });
    const csvData = csvParser.parse(jsonData);

    res.setHeader("Content-Type", "text/csv");
    res.setHeader("Content-Disposition", "attachment; filename=measurements.csv");

    //res.status(200).end(csvData);
    return res.status(200).send(csvData);
    } else {
        res.status(400).send('No data yet for this feed.\n' );
    }
    

    })
    .catch((err) => {
        console.log("couldn't download csv!");
        res.status(400).send('Error\n' );
       });

}

exports.postNewMeasurement = function(req, res, next) {  // NOW BY PUB KEY
    // Extract into variables from request body
    //var {private_key, celcius, humidity } = req.body;
    console.log(req.body);

    var {private_key, temperature_c, humidity_rh, distance_meters, pressure_mbar, battery_volts, gps_lat, gps_lon, gps_alt, distance_meters_1, distance_meters_2, distance_meters_3, temperature_c_1, temperature_c_2, temperature_c_3, voltage_1, voltage_2, voltage_3, aux_1, aux_2, aux_3, log, rssi,node_id, node_name}  = req.body;

    var feed_pubkey = String(req.params.feed_pubkey);

    getFeedDetails(String(req.params.feed_pubkey))
    .then((feed_params) => {

        console.log('got here!');
        console.log(feed_params);

        var feed_id = feed_params.feed_id;

    //console.log(private_key,co2,tempC,humidity,mic,auxPressure,auxTempC,aux001,aux002);

    console.log(private_key);

      const query = {
        text: 'SELECT * FROM feeds WHERE feed_id = $1',
        values: [feed_id],
      }

    console.log('poster key is:');
    console.log(private_key);

   db.query(query, (error, results) => {
    if (error)
        throw error;
        
    console.log("associated private_key is:");
    var key_to_match =results.rows[0].private_key;
    console.log(key_to_match);
    if(private_key==key_to_match) {
        console.log("key match!");

  // Check if values are int, float and float

  var dataValid = (
    typeof co2 == 'number' &&
    typeof tempC == 'number' &&
    typeof humidity == 'number' &&
    typeof mic == 'number' &&
    typeof auxPressure == 'number' &&
    typeof auxTempC == 'number' &&
    typeof aux001 == 'number' &&
    typeof aux002 == 'number'
)

// cheat for now
dataValid=true;

console.log("dataValid=",dataValid)

if (dataValid)  {
    // Create new measurement

    var insertSQL = `INSERT INTO measurements (feed_id, temperature_c, humidity_rh, distance_meters, pressure_mbar, battery_volts, gps_lat, gps_lon, gps_alt, distance_meters_1, distance_meters_2, distance_meters_3, temperature_c_1, temperature_c_2, temperature_c_3, voltage_1, voltage_2, voltage_3, aux_1, aux_2, aux_3, rssi, log, node_id, node_name) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23,$24,$25);`;
    var params = [feed_id, temperature_c, humidity_rh, distance_meters, pressure_mbar, battery_volts, gps_lat, gps_lon, gps_alt, distance_meters_1, distance_meters_2, distance_meters_3, temperature_c_1, temperature_c_2, temperature_c_3, voltage_1, voltage_2, voltage_3, aux_1, aux_2, aux_3, rssi, log,node_id,node_name];

    db.query(insertSQL, params, (error, result) => {
        if (error) {
            res.status(400).send(error);
        } else {
            res.status(200).send('Measurement recorded\n');    
        }
    });

} else {
    res.status(400).send('Please check that your data types are correct' );
}

    }
    else{
        console.log("keys don't match!");
        res.status(400).send('Private key mismatch.\n' );

    }
});
    })
    .catch((err) => {
        console.log("couldn't post to that feed id!");
        res.status(400).send("couldn't post to that feed_id!");
       });
}



exports.getLatestMeasurement = function(req, res, next) {
    
    getFeedDetails(String(req.params.feed_pubkey))
    .then((feed_params) => {
        
        var feed_pubkey = req.params.feed_pubkey
        var feed_id = feed_params.feed_id;
        var feed_name = feed_params.name;

    const query = `SELECT * FROM measurements WHERE feed_id = ${feed_id}  ORDER BY timestamp DESC LIMIT 1`;

    db.query(query, (error, results) => {
        if (error)
            throw error;
            res.status(200).json(restructureJSON(feed_pubkey, feed_name, results.rows));
    });
}).catch((err) => {
    console.log("couldn't get latest measurement for this feed!");
   });


}

exports.getNodeIDJSON = (req,res,next) =>  {

    //var parameters = ["id","tempc","humidity","mic","auxpressure","auxtempc","aux001","aux002","created"];

    var feed_pubkey = String(req.params.feed_pubkey);
    var node_id = String(req.params.node_id);
    
    getFeedDetails(String(req.params.feed_pubkey))
    .then((feed_params) => {

    var feed_id = feed_params.feed_id;
    var feed_name = feed_params.name;

        db.query('SELECT * FROM measurements WHERE feed_id = $1 and node_id = $2', [feed_id,node_id], (err, results) => {
            if (err) {
                res.status(500).json({success: false, error:'Sorry, error'});
            } else {
            res.status(200).json(restructureJSON(feed_pubkey, feed_name, results.rows));
            }    
        });
    })
    .catch((err) => {
       console.log("couldn't get measurements for that feed_id!");
       res.status(500).json({success: false, error:'Sorry, error'});
      });
   
}

exports.getNodeIDCSV = function(req, res, next) {  // NOW BY PUB KEY

    var feed_pubkey = String(req.params.feed_pubkey);
    var node_id = String(req.params.node_id);
    
    getFeedDetails(String(req.params.feed_pubkey))
    .then((feed_params) => {
        var feed_id = feed_params.feed_id;

    console.log("feed_pubkey",feed_pubkey);
    console.log(feed_id);

    db.query('SELECT * FROM measurements WHERE feed_id = $1 and node_id = $2', [feed_id,node_id], (err, response) => {

    if (err) {
    console.log("db.query()", err.stack);
    res.status(400).send("Couldn't fetch that feed!");
    }
    
    if (response) {
    
        var ws = fs.createWriteStream('measurements.csv');

        console.log(response);

        if (response.rows.length>0){
    const jsonData = JSON.parse(JSON.stringify(response.rows));
    console.log("\njsonData:", jsonData)
    
    const csvFields = ["id", "feed_id", "created","celcius"];

    const csvParser = new CsvParser({ csvFields });
    const csvData = csvParser.parse(jsonData);

    res.setHeader("Content-Type", "text/csv");
    res.setHeader("Content-Disposition", "attachment; filename=measurements.csv");

    //res.status(200).end(csvData);
    return res.status(200).send(csvData);
    } else {
        res.status(400).send('No data yet for this feed.\n' );
    }
    }
});
    })
    .catch((err) => {
        console.log("couldn't download csv!");
        res.status(400).send("Couldn't fetch that feed!");
       });

}