# belfast-feed 

A NodeJS + Express + Postgres server for capture and simple visualization of environmental data.

The server can be run in the cloud, or locally (e.g. on a Raspberry Pi).  

There are three main steps for installation:

1. Setting up Postgres
2. Creating a ".env" configuration file
3. Installing the NodeJS modules
4. Running the NodeJS server

## 1. Postgres Setup (Linux)

### Installation

```
sudo apt update
sudo apt install postgresql postgresql-contrib
```

### Creating the database and its tables

```
sudo -i -u postgres
postgres@raspberrypi:~$ createdb belfast2
postgres@raspberrypi:~$ psql belfast2


CREATE TABLE feeds(
    feed_id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    public_key VARCHAR(255) UNIQUE,
    private_key VARCHAR(255) 
);

CREATE TABLE measurements(
id SERIAL PRIMARY KEY,
feed_id INT,
temperature_c FLOAT,
humidity_rh FLOAT,
distance_meters FLOAT,
pressure_mbar FLOAT,
battery_volts FLOAT,
gps_lat FLOAT,
gps_lon FLOAT,
gps_alt FLOAT,
distance_meters_1 FLOAT,
distance_meters_2 FLOAT,
distance_meters_3 FLOAT,
temperature_c_1 FLOAT,
temperature_c_2 FLOAT,
temperature_c_3 FLOAT,
voltage_1 FLOAT,
voltage_2 FLOAT,
voltage_3 FLOAT,
aux_1 FLOAT,
aux_2 FLOAT,
aux_3 FLOAT,
log VARCHAR(255),    
created TIMESTAMP DEFAULT NOW(),
CONSTRAINT feed
 FOREIGN KEY(feed_id)
REFERENCES feeds(feed_id)
);
```

### Changing Postgres Password

```
sudo -u postgres psql ALTER USER postgres PASSWORD 'myPassword';
```

## 2. Creating a '.env' configuration file

Copy the file 'example_env' in the top level directory to a file name '.env' in the top level directory.  

Modify it with your particular choice of Postgres password, and your 'base url' (this will be your domain name if you are hosting this server in the cloud; or could be your local IP address if serving it locally).

## 3. Installing NodeJS modules

### Using nvm (suggested)

For easy use of NodeJS, it's recommended to use 'nvm'.  There's a one-line installation script (curl, or bash) that is described [here]().  

belfast-feed has been written to work with Node ver 12; using nvm, you can perform:

```
nvm install 12
```

### Installing the NodeJS modules

```
npm install
```

### 4. Running the server

```
npm start
```

On the command line you should see the IP address and port of the server -- you can navigate to it in the browser now.  


